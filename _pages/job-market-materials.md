---
permalink: /job-market-materials/
title: "Emily Brennan-Moran"
layout: single
author_profile: true
classes: wide
---

I am a PhD Candidate in the Department of Communication and member of the Graduate School's Royster Society of Fellows at the University of North Carolina at Chapel Hill. I hold a bachelor's degree in Religion from Emory University and a master's degree in Rhetoric and Composition from the University of Central Florida. I teach in the areas of rhetoric and performance studies, with additional experience teaching public speaking and freshman composition. My research focuses on memory, performance, and the complexities of naming the dead in commemorative contexts. I am broadly interested in rhetorical theory and criticism, performance in/of everyday life, memory studies, and critical cultural theory. My work appears in *Text and Performance Quarterly* and *Liminalities: A Journal of Performance Studies*. 

## Publications

Brennan-Moran, Emily. [“Ghosted (I went looking for a haunting).”](https://www.tandfonline.com/doi/abs/10.1080/10462937.2019.1618492) *Text and Performance Quarterly*, vol. 39, no. 3, 2019, pp. 268-284. 

Brennan-Moran, Emily. [“An Offering: Meditations with Walter Benjamin.”](http://liminalities.net/15-1/Offering/) *Liminalities: A Journal of Performance Studies*, vol. 15, no. 1, 2019.  

## Contact 

Email: [ecbrenn@live.unc.edu](mailto:ecbrenn@live.unc.edu) 

<br/> 

---

<br/>


# Job Market Materials

## CV

[Click here to download my CV](../assets/docs/Brennan-Moran-CV.pdf)

## Research Statement

[Click here to download my Research Statement](../assets/docs/Brennan-Moran-Research-Statement.pdf)

## Teaching Statement

[Click here to download my Teaching Statement](../assets/docs/Brennan-Moran-Teaching-Statement.pdf)

## Summary of Course Evaluations

[Click here to download a summary of my Teaching Evaluations](../assets/docs/Brennan-Moran-Summary-of-Teaching-Evaluations.pdf)
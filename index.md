---
layout: single
author_profile: true
classes: wide
---

# Emily Brennan-Moran


I am an assistant professor in the Department of Communication at George Mason University. I hold a PhD in Communication from the University of North Carolina at Chapel Hill, an MA in Rhetoric and Composition from the University of Central Florida, and a BA in Religion from Emory University. 

My research focuses on the ethics and politics of commemoration. I am broadly interested in performances in/of everyday life, memory studies, critical cultural theory, and rhetorical theory and criticism. My current book project explores the place of the proper name in memorializing the dead. My work appears in *Text and Performance Quarterly* and *Liminalities: A Journal of Performance Studies*. 

## Publications

In Press: Ghosal, Torsa and **Emily Brennan-Moran**. “Excessive Tactility of 9/11 Book Memorials.” *The Experimental Book Object*, edited by Sami Sjöberg, Arja Karhunmaa, and Mikko Keskinen, Routledge, anticipated 2023. 

Brennan-Moran, Emily. "Out Past Metonymy in the New Jewish Cemetery, Lublin." *Text and Performance Quarterly*, vol. 43, no. 1, 2023, pp. 1-20. 

Brennan-Moran, Emily. “Ghosted (I went looking for a haunting).” *Text and Performance Quarterly*, vol. 39, no. 3, 2019, pp. 268-284. 

Brennan-Moran, Emily. “An Offering: Meditations with Walter Benjamin.” *Liminalities: A Journal of Performance Studies*, vol. 15, no. 1, 2019.  

Guenzel, Steffen, Daniel S. Murphree, and **Emily Brennan**. “Re-Envisioning the Brown University Model: Embedding a Disciplinary Writing Consultant in an Introductory U.S. History Course.” *Praxis: A Writing Center Journal*, vol. 12, no. 1, 2014, pp. 70-76.

## Contact 

Email: [emoran6@gmu.edu](mailto:emoran6@gmu.edu) 

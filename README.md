## Project Overview

This repository hosts the code for Emily Brennan Moran's professional website. 

## Website Link

You can view the website [here](https://ecbrenn.gitlab.io)